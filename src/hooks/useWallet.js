import AsyncStorage from '@react-native-async-storage/async-storage';
import React, {useState, useEffect, createContext} from 'react';

export const WalletContext = createContext({})

export const WalletProvider = ({ children }) => {
    const [wallet, setWallet] = useState()
    const [walletDetails, setWalletDetails] = useState(false)

    useEffect(() => {
      getWallet()
      return () => wallet
    }, [])


    const getWallet = async() => {
        const walletDetails = await AsyncStorage.getItem("walletDetails")
        
        if(walletDetails){
          await setWallet(true)
          await setWalletDetails(JSON.parse(walletDetails))
        }
    }

    const clearWallet = async() => {
      await AsyncStorage.clear()
      await setWallet(false)
      await setWalletDetails(false)
    }
    return (
      <WalletContext.Provider value={{wallet, walletDetails, setWallet, setWalletDetails, clearWallet}}>
        {children}
      </WalletContext.Provider>
    )
}