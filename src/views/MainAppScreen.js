import React, {useContext, useEffect, useState} from 'react';
import  {
    withStyles,
    Layout,
    BottomNavigation, BottomNavigationTab, Icon,
     Text,
     Spinner,
} from '@ui-kitten/components';
import {
    View
} from 'react-native';
import { SvgCssUri } from 'react-native-svg';
import Clipboard from '@react-native-community/clipboard';
import Snackbar from 'react-native-snackbar';
import { GetBalance } from '../connections/BackendApi'
import {WalletContext} from '../hooks/useWallet'
import {strings} from '../utils/i18n'

const MainAppScreenComponent = ({
    eva
}) => {
    const pulseIconRef = React.useRef();
    React.useEffect(() => {
        if(pulseIconRef.current){
            pulseIconRef.current.startAnimation();
        }
    }, []);

    const {wallet, walletDetails} = useContext(WalletContext)
    const [balance, setBalance] = useState(0.00)
    const [fetching, setFetching] = useState(true);

    useEffect(() => {
        request();
    }, [])

    const request = async() => {
        if(walletDetails){
            const resp = await GetBalance(walletDetails.seed_phrase)
            if(resp && resp.balance){
                setBalance(resp.balance) 
            }
        }
        else{
            setBalance(0.00) 
        }
        setFetching(false)
    }

    const copyToClipBoard = async() => {
        await Clipboard.setString(walletDetails.XCH_address);
        pulseIconRef.current.startAnimation();
        Snackbar.show({
            text: strings("MainAppScreen.clipboard_copy"),
            duration: Snackbar.LENGTH_SHORT,
        });
    }

    return (
        <>
            <Layout style={eva.style.container}>
                {wallet &&
                <View style={eva.style.viewContainer}>
                    <Text category='label' style={{ fontSize: 15}}>
                        {strings("MainAppScreen.balance_title")}
                    </Text>
                    <Text category='h4' style={{ marginTop: 20}}>
                       {fetching ? <Spinner size='small' style={{color: 'white'}}/> : `$ ${balance}`}
                    </Text>
                    <View style={eva.style.walletContainer}>
                        <View style={eva.style.card}>
                            <SvgCssUri 
                                height={70}
                                width={70}
                                style={eva.style.walletIcon}
                                uri= 'https://www.chia.net/img/chia_logo.svg'
                            />
                            <Text category='h6' >
                                {fetching ? <Spinner size='large' style={{color: 'white'}}/> : `$ ${balance}`}
                            </Text>
                            <Icon 
                                onPress={() => copyToClipBoard()}
                                fill={eva.theme['color-primary-600']} 
                                style={{ height: 30, width: 30}} 
                                name='clipboard' 
                                ref={pulseIconRef}
                                animation='pulse'
                            />
                        </View>
                    </View>
                </View>}
                {!wallet && <Text category='h3'>{strings("MainAppScreen.no_wallet")}</Text>}
            </Layout>
        </>
    )
}

const MainAppScreen = withStyles(MainAppScreenComponent, theme => ({
    container: {
      flex: 1,
      
    },
    topBar: {
        backgroundColor: theme['color-primary-100']
    },
    bottomNavigation: {
        paddingVertical: 8,
        height: 20
    },
    icon: {
        width: 10,
        height: 10,
        color: 'green',
    },
    viewContainer: {
        flex: 1,
        top: 20,
        paddingHorizontal: 20
    },
    walletContainer: {
        marginTop: 40,
        display: 'flex',
        padding: 10,
        elevation: 2,
    },
    card: {
        display: 'flex',
        flexDirection :'row',
        // margin: 2,
        alignItems: 'center',
        justifyContent: 'space-between',
        borderBottom: 2,
        borderColor: 'green'
    }
}));

export default MainAppScreen;

